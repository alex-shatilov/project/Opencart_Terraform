## Полезные ссылки
1. https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart
2. https://www.youtube.com/watch?v=YcJ9IeukJL8
3. https://kodekloud.com/courses/lab-terraform-for-beginners/
4. https://cloud.yandex.ru/docs/managed-kubernetes/operations/kubernetes-cluster/kubernetes-cluster-create
5. https://cloud.yandex.com/en/docs/compute/concepts/vm
6. https://cloud.yandex.com/en/docs/managed-postgresql/quickstart
7. https://cloud.yandex.ru/docs/storage/
8. https://cloud.yandex.ru/docs/network-load-balancer/
9. https://cloud.yandex.ru/docs/managed-kafka/
10. http://docs.opencart.com/en-gb/introduction/


# Чекпоинты: 

## Light: 
0. Установить на локальную машину Terraform 
1. Инициировать Terraform в новой директории и настроить его подключение к Яндекс.Облаку 
2. Создать через Terraform виртуальную машину с ubuntu , описав всё в конфигурационых файлах .tf 
3. Иницировать в(-репозиторий и сохранить в него ваш инфраструктурный код Terraform 

## Medium: 
1. Вынести все параметризуемые значения в переменные .tfvars 
2. Вынести .tfstate в защищённый Yandex Object Storage 
3. Описать и развернуть кластер managed Kubernetes в Yandex Cloud 
4. Описать и развернуть кластер managed PostgreSQL в Yandex Cloud 
5. Разместить онлайн-магазин OpenCart в контейнерах Kubernetes 

## Hard: 

1. Сконфигурировать балансировщик Yandex Network Load Balancer для приложения 
2. Настроить созданную ранее ВМ для доступа в закрытую сеть с Kubernetes для администрирования, сделать кластер Kubernetes недоступным из публичных сетей 
3. Подготовить для дальнейшего развития распределённой облачной инфраструктуры Managed Kafka (развернуть минимальный кластер)

# Opencart_Terraform
https://github.com/opencart/opencart
